# Developer guide

## Getting started

Contributions to this service are welcome.

A contribution can be either be a patch or a minor/major feature. Patches include bugfixes and small changes to 
the code. Features can either be minor or major developments and can include potentially disruptive changes.

## Development cycle

Makefile targets have been included to facilitate easier and more consistent development. The general recipe is as 
follows:

1. Depending on the fix type, create a new major/minor/patch branch, e.g. 
    ```bash
    make patch-branch NAME=some-name
    ```
    Note that this both creates and checkouts the branch.

2. Make your changes.

3. Add your changes to the branch:
    ```bash
   git add ...
    ```
   
4. Either commit the changes manually (if no version increment is needed) or bump the version and commit, entering a 
   commit message when prompted:
    ```bash
   make bump-and-commit
    ```
   
5. Push the changes upstream when ready:
    ```bash
   make push
    ```

### Adding a new ingestion backend

To incorporate a new ingestion backend, create a new class derived from the 
`ingestion.backends.backend.IngestionBackend` base class and override the `upload` and `add_metadata_to_existing_file` 
methods, ensuring that you maintain the base function's signatures for these methods.

You will also need to amend the `srcnet-tools-ingest` "binary" and `ingestion.ingest.Ingest` class constructor to 
accommodate the new class, as well as adding an entry to the `ingestion.backends.backend.IngestionBackendTypes` 
enumerator.

## Documentation

There is a Makefile target for generating documentation locally:

```bash
make docs
```

but you will need to ensure the necessary sphinx extensions are installed as these are not included in the core 
requirements.

# FAQ
 
## What data format does it have to be for the daemon? XML, JSON or something else?

Currently it works only with json, but there's no reason it couldn't be extended to work with XML. [This](https://gitlab.com/ska-telescope/src/ska-src-ingestion/-/raw/main/etc/schemas/metadata.json?ref_type=heads) is the (simple) schema of the file it expects.

## Does the daemon support the full v1.1 ObsCore DataModel (described in appendix B p.40)? Or are there some restrictions?

The daemon itself doesn't really care, all it's doing is setting the metadata in the DDM (Rucio) database. There's a database trigger that pulls the metadata from this database and reorganises it into the appropriate  RDBMS table schema for the IVOA services to be able to use it.

## Does the daemon expect a minimum set of ObsCore fields (like e.g. the ObsTAP mandatory fields)?

For this database trigger to fire, and subsequently for entries to be seen in IVOA services, such as our dachs SCS service, you must specify the mandatory fields [here](https://gitlab.com/ska-telescope/src/ska-rucio-ivoa-integration/-/blob/330c5c8612ae42e107225523617781c39bd727ae/postgres-metadata/etc/init/dachs/02-rucio.sql#L55-60)

## Do I need to provide additional information to the daemon which are not ObsCore related? If yes, which?

See the [schema](https://gitlab.com/ska-telescope/src/ska-src-ingestion/-/raw/main/etc/schemas/metadata.json?ref_type=heads). You need the `namespace` and `name` of the file, and also `lifetime`. The first two are used to construct the data identifier in the datalake, and the latter (in seconds) is the lifetime of the replica that will be created for the data.

# What are the mandatory JSON fields for ingestion?

`name` and `namespace` do need to be defined. `name` should be set to whatever you want the associated file to be stored at under the namespace in the DDM. name s must be unique with a namespace. We can create namespaces or use an existing relevant one.

`lifetime` (in seconds) is also, otherwise files will never be removed from the DDM. Setting it to something really long is OK.

`meta contains any JSON you want. If it happens to contain all the mandatory obscore fields the entry will be propagated so that it's visible in our IVOA DaCHS service`

# How do I set `rucio_did_scope` and `rucio_did_name` for ObsCore propagation?

You don't need to. A database trigger handles this. If a file with metadata is ingested, and this metadata includes the mandatory ObsCore fields, the rucio.obscore table will be automatically added to with an entry (and with these fields prepopulated).

# How is the `s_region` field populated for ObsCore propagation?

It has spoly type, so a string like e.g. {(0,0),(1,0),(1,1)}.

# Have you defined `dataproduct_suptype` types yet (since they're specific to an ObsTAP service)?

Nope nothing defined as yet.

# How do I populate the obs\_publisher\_did field for ObsCore propagation?. What are the values for path, query and fragment?

The obs\_publisher\_did path does have a currently preferred form that has the query part of the URI constructed as namespace:name (basically the Rucio data identifier; the same fields you will be setting in the metadata for namespace/scope and name).

 

include .env

build-image-rucio:
	@docker build . -f Dockerfile.rucio \
	--build-arg BASE_RUCIO_CLIENT_IMAGE=$(BASE_RUCIO_CLIENT_IMAGE) \
	--build-arg BASE_RUCIO_CLIENT_TAG=$(BASE_RUCIO_CLIENT_TAG) \
	--tag $(IMAGE_NAME):$(IMAGE_TAG)

bump-and-commit:
	@cd etc/scripts && bash increment-app-version.sh `git branch | grep "*" | awk -F'[*-]' '{ print $$2 }' | tr -d ' '`
	@git add VERSION
	@git commit

docs:
	@cd doc && make clean && make html

major-branch:
	@test -n "$(NAME)"
	@echo "making major branch \"$(NAME)\""
	@git branch major-$(NAME)
	@git checkout major-$(NAME)

minor-branch:
	@test -n "$(NAME)"
	@echo "making minor branch \"$(NAME)\""
	git branch minor-$(NAME)
	git checkout minor-$(NAME)

patch-branch:
	@test -n "$(NAME)"
	@echo "making patch branch \"$(NAME)\""
	@git branch patch-$(NAME)
	@git checkout patch-$(NAME)

push:
	@git push origin `git branch | grep "*" | awk -F'[*]' '{ print $$2 }' | tr -d ' '`

start-ingest-service-rucio:
	@docker-compose -f docker-compose.rucio.yml up

start-ingest-service-rucio-dev:
	@docker-compose -f docker-compose.rucio.dev.yml -f docker-compose.rucio.yml up

stop-ingest-service-rucio:
	@docker-compose -f docker-compose.rucio.yml down

stop-ingest-service-rucio-dev:
	@docker-compose -f docker-compose.rucio.dev.yml -f docker-compose.rucio.yml down

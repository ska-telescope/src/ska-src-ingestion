import os
import sys

# Configuration file for the Sphinx documentation builder.
#
# For the full list of built-in configuration values, see the documentation:
# https://www.sphinx-doc.org/en/master/usage/configuration.html

# -- Project information -----------------------------------------------------
# https://www.sphinx-doc.org/en/master/usage/configuration.html#project-information

project = 'ska-src-dm-di-ingestor'
copyright = '2024, SKAO'

# -- General configuration ---------------------------------------------------
# https://www.sphinx-doc.org/en/master/usage/configuration.html#general-configuration

extensions = ['sphinxcontrib.mermaid', 'sphinxcontrib.plantuml', 'myst_parser', 'sphinxarg.ext']

templates_path = ['_templates']
exclude_patterns = ['_build', 'Thumbs.db', '.DS_Store']

# -- Options for HTML output -------------------------------------------------
# https://www.sphinx-doc.org/en/master/usage/configuration.html#options-for-html-output

html_theme = 'sphinx_rtd_theme'
html_static_path = ['_static']
html_css_files = [
    'css/custom.css',
]

# -- Options for MD ----------------------------------------------------------
source_suffix = ['.rst', '.md']

# -- Options for apidoc ------------------------------------------------------
autoapi_dirs = '../src/ingestor'
autoapi_member_order = 'alphabetical'
autoapi_options = ['members', 'undoc-members', 'private-members', 'show-inheritance', 
        'show-inheritance-diagram', 'show-module-summary', 'special-members', 'imported-members']

# -- Options for plantuml ----------------------------------------------------
plantuml = '/usr/bin/plantuml'

# -- Options for Mermaid -----------------------------------------------------
myst_fence_as_directive = ["mermaid"]
# https://pypi.org/project/sphinxcontrib-mermaid/
mermaid_init_js = '''mermaid.initialize({
      startOnLoad:true,
      "theme": "default",
      "sequence": {
        "noteAlign": "left",
        "noteFontFamily": "verdana",
        "showSequenceNumbers": true,
        "noteMargin": 20,
        "rightAngles": true
      }
    });'''

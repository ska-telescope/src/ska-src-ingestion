.. ska-src-dm-di-ingestor documentation master file, created by
   sphinx-quickstart on Thu Oct 19 13:55:40 2023.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to ska-src-dm-di-ingestor's developer documentation!
===================================================================

.. toctree::
   :maxdepth: 2

   readme.md
   contributing.md
   srcnet-tools-ingest


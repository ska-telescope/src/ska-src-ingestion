#!/bin/bash

export PYTHONWARNINGS="ignore:Unverified HTTPS request"

get_token () {
  # authn/z
  if [ "${RUCIO_CFG_AUTH_TYPE,,}" == 'oidc' ]
  then
    if [ -f "/tmp/access_token" ]                                                                                           # if access token token has been volume mounted
    then
      echo "proceeding with oidc authentication using existing mounted token..."
      cp "/tmp/access_token" "/tmp/tmp_auth_token_for_account_$RUCIO_CFG_ACCOUNT"
    elif [ -v OIDC_CLIENT_ID ] && [ -v OIDC_CLIENT_SECRET ] && [ -v OIDC_TOKEN_ENDPOINT ] && [ -v RUCIO_CFG_OIDC_SCOPE ] && \
      [ -v RUCIO_CFG_OIDC_AUDIENCE ] && [ -v RUCIO_CFG_ACCOUNT ]                                                            # if IAM client's credentials have been passed in (i.e. usuing a service client w/ client_credentials flow)
    then
      echo "proceeding with oidc authentication via passed client credentials..."
      curl -s -u "$OIDC_CLIENT_ID:$OIDC_CLIENT_SECRET" -d grant_type=client_credentials -d scope="$RUCIO_CFG_OIDC_SCOPE" \
        -d audience="$RUCIO_CFG_OIDC_AUDIENCE" $OIDC_TOKEN_ENDPOINT \
        | jq -j '.access_token' > "/tmp/tmp_auth_token_for_account_$RUCIO_CFG_ACCOUNT"
    elif [ -v OIDC_AGENT_AUTH_CLIENT_CFG_VALUE ] && [ -v OIDC_AGENT_AUTH_CLIENT_CFG_PASSWORD ] && [ -v RUCIO_CFG_ACCOUNT ]  # if oidc-agent config is being passed in as a value (e.g. from a k8s secret)
    then
      echo "proceeding with oidc authentication via passed oidc-agent values..."
      # initialise oidc-agent
      # n.b. this assumes that the configuration has a refresh token attached to it with infinite lifetime
      eval "$(oidc-agent-service use)"
      mkdir -p ~/.oidc-agent
      # copy across the auth client configuration (-e to interpolate newline characters)
      echo -e "$OIDC_AGENT_AUTH_CLIENT_CFG_VALUE" > ~/.oidc-agent/rucio-auth
      # add configuration to oidc-agent
      oidc-add --pw-env=OIDC_AGENT_AUTH_CLIENT_CFG_PASSWORD rucio-auth
      # get client name (can be different to short name used by oidc-agent)
      export OIDC_CLIENT_NAME=$(oidc-gen --pw-env=OIDC_AGENT_AUTH_CLIENT_CFG_PASSWORD -p rucio-auth | jq -r .name)
      # retrieve token from oidc-agent; place in file for Rucio client and set env BEARER_TOKEN (for Gfal2 client)
      export BEARER_TOKEN=`oidc-token --scope "$RUCIO_CFG_OIDC_SCOPE" --aud "$RUCIO_CFG_OIDC_AUDIENCE" $OIDC_CLIENT_NAME`
      echo $BEARER_TOKEN > "/tmp/tmp_auth_token_for_account_$RUCIO_CFG_ACCOUNT"
    elif [ -v OIDC_ACCESS_TOKEN ] && [ -v RUCIO_CFG_ACCOUNT ]                                                               # if access token is being passed in directly
    then
      echo "proceeding with oidc authentication using an access token..."
      echo "$OIDC_ACCESS_TOKEN" > "/tmp/tmp_auth_token_for_account_$RUCIO_CFG_ACCOUNT"
      # Gfal client expects BEARER_TOKEN env var
      export BEARER_TOKEN=$OIDC_ACCESS_TOKEN
    else
      echo "requested oidc auth but couldn't find the necessary environment variables to instigate"
      env
      exit 1
    fi
    tr -d '\n' < "/tmp/tmp_auth_token_for_account_$RUCIO_CFG_ACCOUNT" > "/tmp/auth_token_for_account_$RUCIO_CFG_ACCOUNT"
    # move this token to the location expected by Rucio
    mkdir -p /tmp/root/.rucio_root/
    mv "/tmp/auth_token_for_account_$RUCIO_CFG_ACCOUNT" /tmp/root/.rucio_root/
  fi
}

# init rucio (generates rucio.cfg)
/etc/profile.d/rucio_init.sh

# the oidc configuration has a certs path of /etc/ssl/certs/ca-certificates.crt, need it to be
# /etc/ssl/certs/ca-bundle.crt (different distros)
ln -s /etc/ssl/certs/ca-bundle.crt /etc/ssl/certs/ca-certificates.crt

# get a token ad-hoc
get_token

# do a simple whoami query
rucio whoami

cmd="--frequency $FREQUENCY --batch-size $BATCH_SIZE --metadata-suffix $METADATA_SUFFIX \
  --n-processes $N_PROCESSES --ingestion-backend-name $INGESTION_BACKEND_NAME \
  --rucio-ingest-rse-name $RUCIO_INGEST_RSE_NAME"
if [ ! -z "$RUCIO_PFN_BASEPATH" ]; then
  cmd+=" --rucio-pfn-basepath $RUCIO_PFN_BASEPATH"
fi
if [ ! -z "DEBUG" -a "$DEBUG" == "true" ]; then
  cmd+=' --debug'
fi

if [ ! -z "AUTO_START" -a "$AUTO_START" == "true" ]; then
  # run the ingest service in the background
  echo $cmd | xargs srcnet-tools-ingest &
else
  echo "To start the ingestion service, run srcnet-tools-ingest $cmd"
fi

# try get a new token if it's near expiring (within 10mins)
#
# - if a token is volume mounted at /tmp/access_token, it will have to have been refreshed by some process on the host.
#
# - if OIDC_AGENT* credentials were passed and a valid refresh token exists in the corresponding session, the token
# should be automatically refreshed.
#
# - if only OIDC_ACCESS_TOKEN was passed then the same (expired) token will be reused.
#
while [ true ]
do
  current_access_token=$(cat "/tmp/root/.rucio_root/auth_token_for_account_$RUCIO_CFG_ACCOUNT")
  expires_at_unix=$(jq -R 'split(".") | .[1] | @base64d | fromjson | .exp' <<< $current_access_token)
  current_unix_time=$(date +%s)
  echo "Token expires in $(( ($expires_at_unix - $current_unix_time) / 60 )) minutes"
  if [ $current_unix_time -gt $((expires_at_unix - 600)) ]; then get_token; fi
  sleep 300
done



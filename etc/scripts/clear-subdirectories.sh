#!/bin/bash
# Clear subdirectories of some root directory.

main() {
  local root_dir="$1"
  local depth="$2"

  if [ -z "$root_dir" ]; then
    echo "Usage: $0 <directory> <depth>"
    exit 1
  fi

  if [ -z "depth" ]; then
    echo "Usage: $0 <directory> <depth>"
    exit 1
  fi

  if [[ "$root_dir" == "/" ]]; then
    echo "you don't mean this!"
    exit 1
  fi

  read -p "Clear subdirectories of $root_dir at depth $depth? (yes/no): " choice
  if [[ "$choice" == "yes" ]]; then
      find "$1" -mindepth "$depth" -maxdepth "$depth" -type d | while read -r d; do
          rm -rf $d
      done
  fi
}

main "$1" "$2"
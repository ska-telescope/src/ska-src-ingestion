#!/bin/bash
# Count directories and calculate the total size of each subdirectory in the first-level subdirectories of some root
# directory.

# Function to count directories and calculate the total size of a directory in GB with 3 decimal places
count_directories_and_size() {
  local directory="$1"
  local dir_count=$(find "$directory" -mindepth "$2" -type d \! -empty | wc -l)
  local dir_size_in_kb=$(du -sk "$directory" | awk '{print $1}')
  local dir_size_in_gb=$(echo "scale=3; $dir_size_in_kb / 1024 / 1024" | bc)
  echo "$dir_count $dir_size_in_gb GB"
}

# Function to display a table header
display_table_header() {
  printf "%-30s %-15s %-15s\n" "Directory" "Count" "Total Size (GB)"
  echo "-------------------------------------------------------------"
}

# Function to display a table row
display_table_row() {
  local directory="$1"
  local dir_count="$2"
  local dir_size="$3"
  printf "%-30s %-15s %-15s\n" "`basename $directory`" "$dir_count" "$dir_size"
}

main() {
  local root_dir="$1"
  local depth="$2"

  if [ -z "$root_dir" ]; then
    echo "Usage: $0 <directory> <depth>"
    exit 1
  fi

  if [ -z "depth" ]; then
    echo "Usage: $0 <directory> <depth>"
    exit 1
  fi

  display_table_header

  for dir in "$root_dir"/*; do
    if [ -d "$dir" ]; then
      result=$(count_directories_and_size "$dir" "$depth")
      dir_count=$(echo "$result" | awk '{print $1}')
      dir_size=$(echo "$result" | awk '{print $2}')
      display_table_row "$dir" "$dir_count" "$dir_size"
    fi
  done
}

main "$1" "$2"
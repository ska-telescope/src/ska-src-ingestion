#!/bin/sh
# Increment the application version.

IFS=. read -r major minor patch < ../../VERSION

echo "current version: $major.$minor.$patch"

case "$1" in
patch) tag="$major.$minor.$((patch+1))"; ;;
major) tag="$((major+1)).0.0"; ;;
minor)     tag="$major.$((minor+1)).0"; ;;
*) echo "a semantic version type is required" && break; ;;
esac

echo "new version: $tag"

# Change the application VERSION

echo $tag > ../../VERSION

# Change the helm chart application VERSION

sed -i "s/appVersion:.*/appVersion: $tag/" ../helm/Chart.yaml

# Change the image tag in the dotenv file

sed -i "s/IMAGE_TAG=.*/IMAGE_TAG=$tag/" ../../.env

# Change the image tag in the values.yaml.template

sed -i "s/image_tag:.*/image_tag: $tag/" ../helm/values.yaml.template
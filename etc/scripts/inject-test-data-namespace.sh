#/bin/bash
# Inject some test data into the ingestion staging directory.

export INGEST_STAGING_DIR='/tmp/ingest/staging'
export DATA_SIZE_MB=10
export NAMESPACE='testing'

# create a unique name for the file
name=`uuidgen`

# construct the directory the data and metadata will be put in to
directory="$INGEST_STAGING_DIR/$NAMESPACE/`uuidgen`"
mkdir -p $directory

# create some dummy metadata
metadata='{"namespace": "'$NAMESPACE'", "name": "'$name'", "lifetime": 3600}'

# calculate the file size in bytes
data_size_mb="$DATA_SIZE_MB"
data_size_bytes=$((data_size_mb * 1024 * 1024))
bs=8

# create dummy data
dd if=/dev/urandom of="$directory/$name" bs="$bs" count="$((data_size_bytes / bs ))" status=progress
echo $metadata > $directory/$name.meta

echo "Created $directory/$name with metadata $directory/$name.meta"



#!/usr/bin/env python

import glob

from setuptools import setup, find_packages

with open('requirements.txt') as f:
    requirements = f.read().splitlines()

with open('VERSION') as f:
    version = f.read()

data_files = [
    ('etc', glob.glob('etc/*')),
]
scripts = glob.glob('bin/*')

setup(
    name='ingestor',
    version=version,
    description='A tool for ingesting staging data into a datalake.',
    url='',
    author='rob barnsley',
    author_email='rob.barnsley@skao.int',
    packages=['ingestor.common', 'ingestor.ingestion', 'ingestor.messaging',
              'ingestor.state', 'ingestor.ingestion.backends'],
    package_dir={'': 'src'},
    data_files=data_files,
    scripts=scripts,
    include_package_data=True,
    install_requires=requirements,
    classifiers=[]
)

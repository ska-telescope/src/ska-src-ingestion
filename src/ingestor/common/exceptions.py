import traceback
from functools import wraps


def handle_exceptions(func):
    """ Decorator to handle exceptions. """
    @wraps(func)
    def wrapper(*args, **kwargs):
        try:
            return func(*args, **kwargs)
        except CustomException as e:
            raise Exception(e.message)
        except Exception as e:
            detail = "General error occurred: {}, traceback: {}".format(
                repr(e), ''.join(traceback.format_tb(e.__traceback__)))
            raise Exception(detail)
    return wrapper


class CustomException(Exception):
    """ Class that all custom exceptions must inherit in order for exception to be caught by the
    handle_exceptions decorator.
    """
    pass


class InvalidIngestionBackend(CustomException):
    def __init__(self, requested, supported):
        self.message = "The ingestion backend, {requested}, is not supported ({supported}).".format(
            requested=requested, supported=repr(supported))
        super().__init__(self.message)


class InvalidUploadEndpoint(CustomException):
    def __init__(self):
        self.message = "The upload endpoint has been specified incorrectly."
        super().__init__(self.message)


class NoIngestionBackendSpecified(CustomException):
    def __init__(self):
        self.message = "No ingestion backend has been specified."
        super().__init__(self.message)


class RucioIngestRSENotSpecified(CustomException):
    def __init__(self):
        self.message = "The Rucio ingest RSE has not been specified."
        super().__init__(self.message)


class PFNDoesNotExist(CustomException):
    def __init__(self):
        self.message = "Unable to locate PFN with Gfal, check PFN basepath configured correctly."
        super().__init__(self.message)


class PFNMisconfiguration(CustomException):
    def __init__(self):
        self.message = "Unable to reconcile local and remote files, check PFN basepath configured correctly."
        super().__init__(self.message)
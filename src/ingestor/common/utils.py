import io
import mmap
import zlib
from functools import partial


def adler32(file):
    """
    This function is designed to calculate an Adler-32 checksum in exactly the same
    manner as Rucio: https://github.com/rucio/rucio/blob/master/lib/rucio/common/utils.py

    An Adler-32 checksum is obtained by calculating two 16-bit checksums A and B
    and concatenating their bits into a 32-bit integer. A is the sum of all bytes in the
    stream plus one, and B is the sum of the individual values of A from each step.

    :param file: file name
    :returns: Hexified string, padded to 8 values.
    """

    # adler starting value is _not_ 0
    adler = 1

    try:
        with open(file, 'rb') as f:
            # partial block reads at slightly increased buffer sizes
            for block in iter(partial(f.read, io.DEFAULT_BUFFER_SIZE * 8), b''):
                adler = zlib.adler32(block, adler)

    except Exception as e:
        raise Exception('FATAL - could not get Adler-32 checksum of file %s: %s' % (file, e))

    # backflip on 32bit -- can be removed once everything is fully migrated to 64bit
    if adler < 0:
        adler = adler + 2 ** 32

    return str('%08x' % adler)

if __name__ == "__main__":
    file_path = "test_files/test.txt"
    checksum = adler32(file_path)
    print("Adler-32 checksum (Rucio):", checksum)
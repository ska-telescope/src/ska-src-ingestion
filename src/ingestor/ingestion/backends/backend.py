from abc import ABC, abstractmethod
from enum import Enum


class IngestionBackendTypes(Enum):
    """ An enumerator for ingestion backends. """
    RUCIO = 'rucio'
    RUCIO_NON_DET = 'rucio_non_det'


class IngestionBackend(ABC):

    in_place = False

    @staticmethod
    @abstractmethod
    def upload(endpoint, data_file_paths, metadata):
        """ Upload a file into the DDM.

        :param dict endpoint: A dictionary with parameters describing the endpoint.
        :param list data_file_paths: Paths to the data to be uploaded.
        :param dict metadata: The metadata associated with the data.
        """
        raise NotImplementedError

    @staticmethod
    @abstractmethod
    def add_metadata_to_existing_file(metadata):
        """ Add metadata to an already uploaded file.

        :param dict metadata: The metadata associated with the data.
        """
        raise NotImplementedError
import logging

from rucio.client.didclient import DIDClient
from rucio.client.uploadclient import UploadClient

from ingestor.common.exceptions import handle_exceptions, InvalidUploadEndpoint
from ingestor.ingestion.backends.backend import IngestionBackend, IngestionBackendTypes


class RucioIngestionBackend(IngestionBackend):
    backend_type = IngestionBackendTypes.RUCIO

    @staticmethod
    @handle_exceptions
    def upload(endpoint, data_file_path, metadata):
        """ Upload file(s) into Rucio.

        :param dict endpoint: A dictionary containing the parameters: 'ingest_rse_name'
        :param str data_file_path: Path to the data to be uploaded.
        :param dict metadata: The metadata associated with the data.
        """
        ingest_rse_name = endpoint.get('ingest_rse_name')
        if not ingest_rse_name:
            raise InvalidUploadEndpoint

        upload_client = UploadClient(logger=logging.getLogger())
        # If no dataset specified in metadata, file will not be attached to one
        items = [{
            'path': data_file_path,
            'rse': ingest_rse_name,
            'did_scope': metadata.get('namespace'),
            'did_name': metadata.get('name'),
            'dataset_scope': metadata.get('namespace'),
            'dataset_name': metadata.get('dataset_name', ''),
            'lifetime': metadata.get('lifetime'),
            'register_after_upload': True
        }]
        upload_client.upload(items=items)

    @staticmethod
    @handle_exceptions
    def add_metadata_to_existing_file(metadata):
        """ Add metadata to an already uploaded file.

        :param dict metadata: The metadata associated with the data.
        """
        if not metadata.get('meta', ""):
            logging.info("No metadata to set")
        else:
            did_client = DIDClient(logger=logging.getLogger())
            did_client.set_metadata_bulk(
                scope=metadata.get('namespace'),
                name=metadata.get('name'),
                meta=metadata.get('meta')
            )



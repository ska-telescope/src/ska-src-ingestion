import logging
import os
import urllib.parse
from pathlib import Path

from gfal2 import GError, Gfal2Context
from rucio.client.didclient import DIDClient
from rucio.client.replicaclient import ReplicaClient
from rucio.common.exception import DataIdentifierAlreadyExists

from ingestor.common.exceptions import (InvalidUploadEndpoint,
                                        PFNDoesNotExist,
                                        PFNMisconfiguration,
                                        handle_exceptions)
from ingestor.common.utils import adler32
from ingestor.ingestion.backends.backend import (
    IngestionBackend, IngestionBackendTypes)


class RucioNonDetIngestionBackend(IngestionBackend):
    backend_type = IngestionBackendTypes.RUCIO_NON_DET
    in_place = True

    @staticmethod
    @handle_exceptions
    def upload(endpoint, data_file_path, metadata):
        """ Register file(s) in-place with Rucio.

        :param dict endpoint: A dictionary containing the parameters: 'ingest_rse_name'
        :param str data_file_path: Path to the data to be uploaded.
        :param dict metadata: The metadata associated with the data.
        """

        def get_pfn(pfn_base, data_file_path, base_directory):
            path_base = Path(base_directory)
            path_full = Path(data_file_path)
            path_relative = str(path_full.relative_to(path_base))
            return urllib.parse.urljoin(pfn_base, path_relative)
        
        ingest_rse_name = endpoint.get('ingest_rse_name')
        pfn_basepath = endpoint.get('pfn_basepath')
        base_directory = endpoint.get('base_directory')
        if not ingest_rse_name:
            raise InvalidUploadEndpoint

        replica_client = ReplicaClient(logger=logging.getLogger())
        did_client = DIDClient(logger=logging.getLogger())

        # initialise Gfal2 client; this uses the env var BEARER_TOKEN to auth
        gfal = Gfal2Context()

        # Establish registration properties:
        rse = endpoint.get('ingest_rse_name')
        scope = metadata.get('namespace')
        name = metadata.get('name')
        size = os.stat(data_file_path).st_size
        checksum = adler32(data_file_path)
        pfn = get_pfn(pfn_basepath, data_file_path, base_directory)
        
        logging.info("Registering file with PFN: {}".format(pfn))

        # Verify file exists at the expected PFN
        # We could use os.stat to read locally, but this verifies file is visible to the storage manager
        try:
            gfal_size = gfal.stat(pfn).st_size
        except GError as e:
            logging.critical("Error reading remote file from storage: {}".format(e.message))
            if e.message.startswith("Result HTTP 404"):
                raise PFNDoesNotExist
            raise

        if size != gfal_size:
            logging.critical("File at PFN {} is different to local file at {}".format(pfn, data_file_path))
            raise PFNMisconfiguration

        logging.debug("RSE: {}, Scope: {}, Name: {}, Size: {}, Adler-32 checksum: {}".format(
            rse, scope, name, size, checksum
        ))
        replica_client.add_replica(rse=rse, scope=scope, name=name, bytes_=size, adler32=checksum, pfn=pfn)

        # If a dataset has been specified in the metadata, file should be attached to this dataset
        # Create dataset if doesn't already exist (same behaviour as rucio backend)
        dataset_name = metadata.get("dataset_name", "")
        if dataset_name:
            try:
                did_client.add_did(scope=scope, name=dataset_name, did_type="dataset", lifetime=3600, rse="STFC_STORM")
            except DataIdentifierAlreadyExists:
                logging.info("Dataset {}:{} already exists".format(scope, dataset_name))
            except Exception as e:
                logging.warning("Unable to create requested dataset {}:{}".format(scope, dataset_name))
                raise

            file = {"scope": scope, "name": name, "type": "FILE"}
            did_client.attach_dids(scope, dataset_name, [file])

    @staticmethod
    @handle_exceptions
    def add_metadata_to_existing_file(metadata):
        """ Add metadata to an already uploaded file.

        :param dict metadata: The metadata associated with the data.
        """
        if not metadata.get('meta', ""):
            logging.info("No metadata to set")
        else:
            did_client = DIDClient(logger=logging.getLogger())
            did_client.set_metadata_bulk(
                scope=metadata.get('namespace'),
                name=metadata.get('name'),
                meta=metadata.get('meta')
            )

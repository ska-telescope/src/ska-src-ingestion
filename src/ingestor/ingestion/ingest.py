import asyncio
import logging
import json
import jsonschema
import os
import shutil
import time
import uuid
import warnings
from multiprocessing import get_context
from pathlib import Path

from ingestor.common.exceptions import (InvalidIngestionBackend, NoIngestionBackendSpecified,
                                        RucioIngestRSENotSpecified)
from ingestor.ingestion.path_decomposition import decompose_path_namespace
from ingestor.ingestion.backends.backend import IngestionBackendTypes
from ingestor.messaging.handler import LocalMessageHandler, ElasticMessageHandler
from ingestor.messaging.message import MessageStore
from ingestor.state.state import IngestState
from ingestor.state.machine import IngestStateMachine


class Ingest:
    def __init__(self, base_directory, metadata_schema, metadata_suffix, ingestion_backend=None,
                 ingestion_backend_kwargs={}, path_decomposition_function=decompose_path_namespace, debug=False):
        """ A class for data ingestion into Rucio.

        :param str base_directory: The base directory for the service.
        :param dict metadata_schema: The metadata schema that metadata files must conform to.
        :param str metadata_suffix: The metadata suffix to expect for metadata files.
        :param IngestionBackend ingestion_backend: The ingestion backend to use.
        :param dict ingestion_backend_kwargs: Dictionary of backend-specific parameters.
        :param callable path_decomposition_function: The function that will decompose a staging path into parts.
        :param bool debug: Debug mode?
        """
        self.base_directory = base_directory
        self.metadata_schema = metadata_schema
        self.metadata_suffix = metadata_suffix
        self.ingestion_backend = ingestion_backend
        self.path_decomposition_function = path_decomposition_function
        self.debug = debug

        # parse ingestion backend
        self.ingestion_backend_kwargs = {}
        if not ingestion_backend:
            raise NoIngestionBackendSpecified
        if ingestion_backend.backend_type in [IngestionBackendTypes.RUCIO, IngestionBackendTypes.RUCIO_NON_DET]:
            if not ingestion_backend_kwargs.get('endpoint', {}).get('ingest_rse_name'):
                raise RucioIngestRSENotSpecified
            self.ingestion_backend_kwargs = ingestion_backend_kwargs
        else:
            raise InvalidIngestionBackend(requested=ingestion_backend,
                                          supported=[backend_type.name for backend_type in IngestionBackendTypes])

        # create subdirectories corresponding to all states in IngestState. Files will be moved to these subdirectories
        # as processing proceeds through the state machine states.
        self.ingest_state_subdirectories = {}
        for state in IngestState:
            state_path = os.path.join(base_directory, state.name.lower())
            Path(state_path).mkdir(exist_ok=True)
            self.ingest_state_subdirectories[state] = state_path

        self.directory_to_monitor = self.ingest_state_subdirectories[IngestState.STAGING]   # monitor the STAGING subdir

    def _link_data_to_metadata_by_name(self, iteration_batch_size, recurse_subdirectories=True):
        """ Link data to metadata by name.

        This expects the format:
          - data_name
          - data_name.<metadata_suffix>

        :param int iteration_batch_size: The maximum number of files to process this iteration.
        :param bool recurse_subdirectories: Recurse into subdirectories of the monitored directory.
        :return: A list of dictionary entries in the format [{'data_file_path', 'associated_metadata_file_path'}, ...]
        """
        validated_linked_data_and_metadata_files = {}
        for (root, dirs, files) in os.walk(self.directory_to_monitor, topdown=True):
            # use decomposition function to decompose path to get namespace
            decomposed_path = self.path_decomposition_function(
                staging_directory=self.directory_to_monitor, directory=root)
            if decomposed_path:
                namespace = decomposed_path.get('namespace')
                if not namespace:
                    logging.warning("Couldn't find namespace for path {}, skipping...".format(root))
                    continue
            else:
                logging.warning("Couldn't decompose path {}, skipping...".format(root))
                continue

            # separate out data and metadata files
            files_fullpaths = set([os.path.join(root, f) for f in files])
            files_metadata = set([f for f in files_fullpaths if f.endswith(self.metadata_suffix)])
            files_data = set([f for f in files_fullpaths - files_metadata])

            # pair the data with the metadata
            linked_data_and_metadata_files = {}
            for f in list(files_data)[0:iteration_batch_size]:
                expected_metadata_file_path = '{}.{}'.format(f, self.metadata_suffix.lstrip('.'))
                if expected_metadata_file_path in files_metadata:
                    linked_data_and_metadata_files[f] = expected_metadata_file_path
            if not recurse_subdirectories:
                break

            # validate metadata and check that the namespace in the metadata matches that of the directory
            # where it's been put in the staging area, otherwise a user can arbitrarily put data into whatever
            # namespace they want.
            for data_path, metadata_path in linked_data_and_metadata_files.items():
                if self._validate_metadata(metadata_path, enforce_key_values={'namespace': namespace}):
                    validated_linked_data_and_metadata_files[data_path] = metadata_path
        return validated_linked_data_and_metadata_files

    def _validate_metadata(self, metadata_path, enforce_key_values={}):
        """ Validate metadata conforms to schema and (optionally) check that certain keys match specific values.

        :param str metadata_path: The path to the metadata.
        :param dict enforce_key_values: Dictionary of keys to enforce have specific values.
        """
        # validate metadata against schema
        metadata = {}
        with open(metadata_path) as f:
            try:
                metadata = json.loads(f.read())
            except json.decoder.JSONDecodeError:
                logging.warning("Error loading metadata with path {}".format(metadata_path))
                return False
        try:
            jsonschema.validate(metadata, schema=self.metadata_schema)
        except jsonschema.exceptions.ValidationError:
            logging.warning("Error validating metadata with path {path}".format(metadata_path))
            return False

        # enforce keys if applicable
        for key, value in enforce_key_values.items():
            if metadata.get(key):
                if metadata.get(key) != value:
                    logging.warning("Key {} is not enforced as {}".format(key, value))
                    return False
        return True

    @staticmethod
    def ingest_new_data(staging_data_file_path, staging_metadata_file_path, ingestion_backend, ingestion_backend_kwargs,
                        ingest_state_subdirectories, debug=False, log_name='log'):
        """ A function for ingesting new data/metadata.

        This is spawned as a child process.

        :param str staging_data_file_path: The data file path in the STAGING area.
        :param str staging_metadata_file_path: The metadata file path in the STAGING area.
        :param IngestionBackend ingestion_backend: The ingestion backend to use (supported: RucioIngestionBackend)
        :param dict ingestion_backend_kwargs: Dictionary of backend-specific parameters.
        :param dict ingest_state_subdirectories: A dictionary of subdirectories for each ingest state.
        :param bool debug: Debug mode?
        :param str log_name: The name to use for log files.
        """
        def move_files(current_file_paths, new_directory):
            Path(new_directory).mkdir()
            new_file_paths = {}
            for key, path in current_file_paths.items():
                shutil.move(path, new_directory)
                new_file_paths[key] = os.path.join(new_directory, os.path.basename(path))
            return new_file_paths

        # set up a logger for this child process
        if debug:
            logging.basicConfig(
                level=logging.DEBUG,
                format="%(asctime)s [%(name)s] %(module)10s %(levelname)5s %(process)d\t%(message)s")
        else:
            logging.basicConfig(
                level=logging.INFO,
                format="%(asctime)s [%(name)s] %(module)10s %(levelname)5s %(process)d\t%(message)s")

        # disable some logger warnings
        logging.getLogger("urllib3").setLevel(logging.WARNING)
        warnings.filterwarnings('ignore', message='Unverified HTTPS request')

        # create message store (without handlers) and instantiate the state machine for this ingest.
        message_store = MessageStore()
        state_machine = IngestStateMachine(message_store=message_store)

        # create dictionary to keep track of current file locations
        current_file_paths = {
            "data": staging_data_file_path,
            "metadata": staging_metadata_file_path
        }

        # create unique id for this ingest
        ingest_uuid = str(uuid.uuid4())

        logging.info("Ingesting new file {data_file_path} (metadata: {metadata_file_path}) with identifier "
                     "{identifier}.".format(data_file_path=staging_data_file_path,
                                            metadata_file_path=staging_metadata_file_path,
                                            identifier=ingest_uuid))

        ######################################################
        # STAGING -> INGEST_REQUEST || FAILED_INGEST_REQUEST #
        ######################################################
        try:
            # create target subdirectory to move files into
            # for files that will be uploaded, move to PROCESSING
            # for files that will be registered in-place move to PROCESSED
            if not ingestion_backend.in_place:
                target_directory = os.path.join(ingest_state_subdirectories.get(IngestState.PROCESSING), ingest_uuid)
            else:
                target_directory = os.path.join(ingest_state_subdirectories.get(IngestState.PROCESSED), ingest_uuid)

            # move existing data & metadata files to subdirectory for PROCESSING/PROCESSED
            current_file_paths = move_files(current_file_paths, new_directory=target_directory)

            # touch output and log files in subdirectory for PROCESSING/PROCESSED
            current_file_paths[log_name] = os.path.join(target_directory, log_name)
            Path(current_file_paths[log_name]).touch()

            # add a message handler to the message store and change the ingest state to PROCESSING
            # this has to be before the files are moved as changing state adds a message to the store
            message_store.add_message_handler(LocalMessageHandler(file_path=current_file_paths.get(log_name)))

            # pull out additional messaging handlers from the environment
            #
            if os.getenv("ELASTICSEARCH_URL") and os.getenv("ELASTICSEARCH_INDEX"):
                http_auth_credentials = {}
                if os.getenv("ELASTICSEARCH_USER") and os.getenv("ELASTICSEARCH_PASSWORD"):
                    http_auth_credentials['user'] = os.getenv("ELASTICSEARCH_USER")
                    http_auth_credentials['password'] = os.getenv("ELASTICSEARCH_PASSWORD")
                message_store.add_message_handler(ElasticMessageHandler(
                    url=os.getenv("ELASTICSEARCH_URL"),
                    index=os.getenv("ELASTICSEARCH_INDEX"),
                    http_auth_credentials=http_auth_credentials
                ))

            state_machine.to_processing(ingest_uuid=ingest_uuid)
        except Exception as e:
            logging.critical(repr(e))

            # change the ingest state to FAILED INGEST REQUEST
            state_machine.to_failed_ingest_request(ingest_uuid=ingest_uuid)

            # move to FAILED INGEST REQUEST subdirectory
            failed_ingest_request_directory = os.path.join(
                ingest_state_subdirectories.get(IngestState.FAILED_INGEST_REQUEST), ingest_uuid)
            current_file_paths = move_files(current_file_paths, new_directory=failed_ingest_request_directory)

            return {
                'id': ingest_uuid,
                'state': state_machine.current_state,
                'error_detail': repr(e),
                'file_paths': {**current_file_paths}
            }

        ################################################
        # PROCESSING -> PROCESSED || FAILED_PROCESSING #
        ################################################
        try:
            processing_start = time.time()

            # read metadata (already validated)
            with open(current_file_paths.get('metadata')) as f:
                metadata = json.loads(f.read())

            # move data into backend
            #
            # For data being registered in-place (ingestion_backend.in_place == True),
            # data will already be in the PROCESSED directory
            processing_checkpoint = time.time()
            ingestion_backend.upload(
                endpoint=ingestion_backend_kwargs.get('endpoint'),
                data_file_path=current_file_paths.get('data'),
                metadata=metadata
            )
            message_store.add_message("Uploaded data",
                                      state=state_machine.current_state.name,
                                      ingest_uuid=ingest_uuid,
                                      stage_duration=time.time()-processing_checkpoint)

            # add metadata to this data
            processing_checkpoint = time.time()
            ingestion_backend.add_metadata_to_existing_file(
                metadata=metadata
            )
            message_store.add_message("Added metadata",
                                      state=state_machine.current_state.name,
                                      ingest_uuid=ingest_uuid,
                                      stage_duration=time.time()-processing_checkpoint)

            # move to PROCESSED subdirectory (upload only)
            if not ingestion_backend.in_place:
                successfully_processed_directory = os.path.join(
                    ingest_state_subdirectories.get(IngestState.PROCESSED), ingest_uuid)
                current_file_paths = move_files(current_file_paths, new_directory=successfully_processed_directory)

            state_machine.to_successfully_processed(new_log_file_path=current_file_paths.get(log_name),
                                                    ingest_uuid=ingest_uuid,
                                                    duration=time.time()-processing_start)
        except Exception as e:
            logging.critical(repr(e))
            message_store.add_message(repr(e),
                                      state=state_machine.current_state.name,
                                      ingest_uuid=ingest_uuid)

            # change the ingest state to FAILED PROCESSING
            # this has to be before the files are moved as changing state adds a message to the store
            state_machine.to_failed_processing(new_log_file_path=current_file_paths.get(log_name),
                                               ingest_uuid=ingest_uuid)

            # move to FAILED_PROCESSING subdirectory
            failed_processing_directory = os.path.join(
                ingest_state_subdirectories.get(IngestState.FAILED_PROCESSING), ingest_uuid)
            current_file_paths = move_files(current_file_paths, new_directory=failed_processing_directory)

            return {
                'id': ingest_uuid,
                'state': state_machine.current_state,
                'error_detail': repr(e),
                'file_paths': {**current_file_paths}
            }
        logging.info("Ingested {data_file_path} (metadata: {metadata_file_path}) with identifier {identifier}".format(
            data_file_path=staging_data_file_path, metadata_file_path=staging_metadata_file_path,
            identifier=ingest_uuid))

        unsent_messages = message_store.count_messages()
        if unsent_messages > 0:
            logging.warning("There are {} unsent messages in this message store".format(unsent_messages))
            #FIXME: need to persist these messages for sending later.
        return {
            'id': ingest_uuid,
            'state': state_machine.current_state,
            'file_paths': {**current_file_paths}
        }

    async def wait_for_new_data(self, frequency_s, iteration_batch_size, n_processes, output_name='result'):
        """ Wait for new data and begin a new ingestion process if it's found.

        This is the parent function that runs within an asyncio loop.

        :param int frequency_s: The interval at which non-state changes are refreshed.
        :param int iteration_batch_size: The (maximum) number of files to process per iteration.
        :param int n_processes: The (maximum) number of concurrent processes to use.
        :param str output_name: The name of the output file created per ingest.

        """
        while True:
            # get list of files for this iteration and link with metadata
            linked_data_and_metadata_files = self._link_data_to_metadata_by_name(
                iteration_batch_size=iteration_batch_size, recurse_subdirectories=True)

            # if there are some new files to process...
            if len(linked_data_and_metadata_files) > 0:
                # create process pool for asynchronous processing of file pairs
                ctx = get_context('spawn')
                with ctx.Pool(processes=n_processes) as pool:
                    logging.info("Found the following new files in this iteration: {}".format(
                        repr(linked_data_and_metadata_files)))
                    processes = []
                    # add asynchronous processes to the pool
                    for data_file_staging_path, metadata_file_staging_path in linked_data_and_metadata_files.items():
                        # process
                        processes.append(pool.apply_async(self.ingest_new_data, (data_file_staging_path,
                                                                                 metadata_file_staging_path,
                                                                                 self.ingestion_backend,
                                                                                 self.ingestion_backend_kwargs,
                                                                                 self.ingest_state_subdirectories,
                                                                                 self.debug)))
                    # evaluate process results and act accordingly
                    while processes:
                        for process in processes:
                            if process.ready():
                                rtn = process.get()

                                # create output
                                ingest_id = rtn.get('id')
                                last_state = rtn.get('state')
                                output_file_path = os.path.join(self.ingest_state_subdirectories.get(last_state),
                                                                ingest_id, output_name)
                                rtn['file_paths'][output_name] = output_file_path
                                with open(output_file_path, 'w') as f:
                                    rtn['state'] = rtn.get('state').name
                                    json.dump(rtn, f)

                                # remove the process from the list
                                processes.remove(process)
            else:
                logging.info("No new files found for this iteration".format(frequency_s))
            logging.info("Sleeping for {}s...".format(frequency_s))
            await asyncio.sleep(frequency_s)

    def start(self, frequency_s, iteration_batch_size, n_processes):
        """ Start an ingestion service.

        :param int frequency_s: The interval at which non-state changes are refreshed.
        :param int iteration_batch_size: The (maximum) number of files to process per iteration.
        :param int n_processes: The (maximum) number of concurrent processes to use.
        """
        loop = asyncio.get_event_loop()
        loop.create_task(self.wait_for_new_data(frequency_s=frequency_s, iteration_batch_size=iteration_batch_size,
                                                n_processes=n_processes))
        logging.info("Starting loop for directory {directory}...".format(directory=self.directory_to_monitor))
        try:
            loop.run_forever()
        except KeyboardInterrupt:
            logging.info("Received KeyboardInterrupt, stopping...")





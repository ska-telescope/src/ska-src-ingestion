# Path decomposition functions.
#
# These pluggable functions are used to decompose a path in the staging directory into constituent parts for use by
# the ingestion engine.
#
# See the stub for the expected signature and returns.
#
import os


def decompose_path_stub(staging_directory, directory):
    """ Decompose a staged directory into parts to get the namespace.

    :param str staging_directory: The staging directory.
    :param str directory: The directory to decompose.
    """
    if staging_directory == directory:
        return False
    return {
        'namespace': "dummy_namespace_return"
    }


def decompose_path_namespace(staging_directory, directory):
    """ Decompose function assuming staged directory is constructed like /staging_directory/namespace/...

    :param str staging_directory: The staging directory.
    :param str directory: The directory to decompose.
    """
    if staging_directory == directory:
        return False
    parts = os.path.normpath(os.path.relpath(directory, staging_directory)).split(os.sep)
    return {
        'namespace': parts[0]
    }

import logging

from abc import ABC, abstractmethod
from datetime import datetime
from functools import wraps
from elasticsearch import Elasticsearch


def mandatory(func):
    """ Decorator to make the successful processing of a message in a handler mandatory. """
    @wraps(func)
    def wrapper(*args, **kwargs):
        try:
            func(*args, **kwargs)
            return True
        except Exception as e:
            logging.warning(repr(e))
            return False
    return wrapper


def nonmandatory(func):
    """ Decorator to make the successful processing of a message in a handler non-mandatory. """
    @wraps(func)
    def wrapper(*args, **kwargs):
        try:
            func(*args, **kwargs)
            return True
        except Exception as e:
            logging.warning(repr(e))
            return True
    return wrapper


class MessageHandler(ABC):
    @abstractmethod
    def process_message(self, message):
        """ Do something with a message.

        :param Message message: The message.
        """
        raise NotImplementedError


class LocalMessageHandler(MessageHandler):
    def __init__(self, file_path=None):
        self._file_path = file_path

    @nonmandatory
    def process_message(self, message):
        """ Add a message to a log file.

        This handler is non-mandatory as if the message fails to send, it cannot be sent later.

        :param Message message: The message.
        """
        with open(self.file_path, 'a') as f:
            f.write("{time}\t{message}\n".format(time=message.time, message=message.substituted_message))

    @property
    def file_path(self):
        """ Get the file path for the log file. """
        return self._file_path

    @file_path.setter
    def file_path(self, new_file_path):
        """ Set the path for the log file.

        :param str new_file_path: The new file path to change to.
        """
        self._file_path = new_file_path


class ElasticMessageHandler(MessageHandler):
    def __init__(self, url, index, http_auth_credentials=None):
        self.elastic_url = url
        self.elastic_index = index

        if http_auth_credentials:
            self.es = Elasticsearch([self.elastic_url], http_auth=(
                http_auth_credentials.get('user'), http_auth_credentials.get('password')))
        else:
            self.es = Elasticsearch([self.elastic_url])

        # this logger is very chatty...
        logging.getLogger('elastic_transport.transport').setLevel(logging.CRITICAL)

    @mandatory
    def process_message(self, message):
        """ Send a message to an elasticsearch instance.

        :param Message message: The message.
        """
        self.es.index(index=self.elastic_index, id=None, body={
            "created_at": datetime.now().isoformat(),
            "message": message.substituted_message,
            **message.kwargs
        })

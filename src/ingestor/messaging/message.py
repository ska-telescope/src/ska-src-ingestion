import logging
import time


class Message:
    """ A message. """
    def __init__(self, *args, **kwargs):
        self.time = time.time()
        self.template_message = args[0]         # expects the message string as the first positional argument
        self.kwargs = kwargs
        try:
            self.substituted_message = self.template_message.format(**kwargs)
        except KeyError as e:
            logging.warning("Key not found for message: {}".format(repr(e)))
            self.substituted_message = ''


class MessageStore:
    """ A store for Messages. """
    messages = {}

    def __init__(self, handlers=[]):
        self.handlers = handlers

        # create a new message "stack" for each handler
        for handler in self.handlers:
            self.messages[handler] = []

    def add_message_handler(self, handler):
        """ Add a new message handler to the store.

        :param MessageHandler handler: The handler to add.
        """
        self.handlers.append(handler)
        self.messages[handler] = []

    def add_message(self, *args, flush=True, **kwargs):
        """ Add a new message to the store.

        :param str args[0]: The message.
        :param bool flush: Attempt to flush message immediately.
        """
        kwargs.pop('flush', None)
        message = Message(*args, **kwargs)
        for handler in self.handlers:
            if flush:
                if not handler.process_message(message):
                    self.messages[handler].append(message)      # depends on whether handler is mandatory/non-mandatory

    def count_messages(self):
        """ Count total of all messages for each handler. """
        total_count = 0
        for handler, messages in self.messages.items():
            total_count += len(messages)
        return total_count

    def flush_all_messages(self):
        """ Call process_message() for each handler on all messages. """
        n_successful = 0
        n_failed = 0
        for handler, messages in self.messages.items():
            for message in messages:
                if handler.process_message(message):
                    n_successful += 1
                    self.messages[handler].remove(message)
                else:
                    n_failed += 1
        return n_successful, n_failed

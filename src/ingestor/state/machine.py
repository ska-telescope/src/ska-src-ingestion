from ingestor.messaging.handler import LocalMessageHandler
from ingestor.state.state import IngestState


class IngestStateMachine:
    """ A state machine to keep track of an ingestion process. """
    _current_state = IngestState.STAGING

    def __init__(self, message_store=None):
        self.message_store = message_store

    def _change_current_state(self, to_state, **kwargs):
        if self.message_store is not None:
            self.message_store.add_message("changed state from {from_state} to {to_state}",
                                           from_state=self.current_state.name, to_state=to_state.name,
                                           **kwargs)
        self.current_state = to_state

    def _update_local_message_handlers_file_path(self, new_log_file_path):
        if self.message_store is not None:
            for handler in self.message_store.handlers:
                if isinstance(handler, LocalMessageHandler):
                    handler.file_path = new_log_file_path

    def to_failed_ingest_request(self, **kwargs):
        """ Change the current state to failed ingest request. """
        self._change_current_state(to_state=IngestState.FAILED_INGEST_REQUEST, **kwargs)

    def to_failed_processing(self, new_log_file_path, **kwargs):
        """ Change the current state to failed processing. """
        self._update_local_message_handlers_file_path(new_log_file_path)
        self._change_current_state(to_state=IngestState.FAILED_PROCESSING, **kwargs)

    def to_successfully_processed(self, new_log_file_path, **kwargs):
        """ Change the current state to successfully processed. """
        self._update_local_message_handlers_file_path(new_log_file_path)
        self._change_current_state(to_state=IngestState.PROCESSED, **kwargs)

    def to_processing(self, **kwargs):
        """ Change the current state to processing. """
        self._change_current_state(to_state=IngestState.PROCESSING, **kwargs)

    @property
    def current_state(self):
        """ Get the current ingestion state machine state. """
        return self._current_state

    @current_state.setter
    def current_state(self, to_state):
        """ Set the current ingestion state machine state.

        :param IngestState to_state: The state to change to.
        """
        self._current_state = to_state

from enum import Enum


class IngestState(Enum):
    """ An enumerator for state machine states. """
    STAGING = 0
    FAILED_INGEST_REQUEST = 1
    PROCESSING = 2
    FAILED_PROCESSING = 3
    PROCESSED = 4
